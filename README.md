# quantum folder

Development in the Photon Quantum (ECS based network solution).
Contains examples of code:
- Perks - asset files and systems
- Tutorial - the same

Quantum uses code generation and doesn't allow 2nd order inheritance, so asset files can look a bit strange.


## unity folder

Contains:
- client part of battle tutorial
- lobby tutorial system
- player deck structure