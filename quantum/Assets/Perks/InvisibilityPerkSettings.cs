﻿using System;
using System.Linq;
using Newtonsoft.Json;
using Photon.Deterministic;

namespace Quantum
{
[Serializable]
public class InvisibilityPerkSettings : EffectSettings
{
    public BaseContainer<BaseGradesWithKey<Grade>, Grade> Container;

    public bool TryGetGradeContainer(string code, int idGrade, out InvisibilityPerkContainer container)
    {
        container = default;
        
        if (!TryGetGrade(code, idGrade, Container, out var grade)) return false;
        
        container = new InvisibilityPerkContainer
        {
            Delay = grade.Delay,
        };

        return true;
    }
    
    public override void SetJSONData(string[] json) =>
        SetJSONData<BaseGradesWithKey<Grade>, Grade, GradeAdapter>(json, Container);
    
    public override int GetSettingsVariantIndex(string code) =>
        GetSettingsVariantIndex(code, Container);

    public override string GetSettingsCodeByVariantIndex(int variantIndex) =>
        GetSettingsCodeByVariantIndex(variantIndex, Container);

    public override bool TryGetParametersForView(string code, int idGrade, out ParameterForView[] parametersForView) =>
        TryGetParametersForView(code, idGrade, Container, out parametersForView);

    [Serializable]
    public class Grade : BaseGrade
    {
        public FP Delay;
    
        public override FP[] ParametersArray => new[] {Delay,};

        public override void SetAdapter<TGradeAdapter>(TGradeAdapter adapter)
        {
            var currentAdapter = (GradeAdapter) (BaseGradeAdapter) adapter;
            Delay = FP.FromFloat_UNSAFE(currentAdapter.Delay);
        }
    }

    [Serializable]
    public class GradeAdapter : BaseGradeAdapter
    {
        public float Delay;
    }
}

public struct InvisibilityPerkContainer
{
    public FP Delay;
}
}