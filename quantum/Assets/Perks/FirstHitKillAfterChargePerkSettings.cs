﻿using System;
using Photon.Deterministic;

namespace Quantum
{
[Serializable]
public class FirstHitKillAfterChargePerkSettings : EffectSettings
{
    public BaseContainer<BaseGradesWithKey<Grade>, Grade> Container;

    public bool TryGetGradeContainer(string code, int idGrade, out FirstHitKillAfterChargePerkContainer container)
    {
        container = default;
        
        if (!TryGetGrade(code, idGrade, Container, out var grade)) return false;
        
        container = new FirstHitKillAfterChargePerkContainer
        {
            Probability = grade.Probability,
            MinDistance = grade.MinDistance,
            Types = grade.Types,
        };

        return true;
    }
    
    public override void SetJSONData(string[] json) =>
        SetJSONData<BaseGradesWithKey<Grade>, Grade, GradeAdapter>(json, Container);
    
    public override int GetSettingsVariantIndex(string code) =>
        GetSettingsVariantIndex(code, Container);

    public override string GetSettingsCodeByVariantIndex(int variantIndex) =>
        GetSettingsCodeByVariantIndex(variantIndex, Container);

    public override bool TryGetParametersForView(string code, int idGrade, out ParameterForView[] parametersForView) =>
        TryGetParametersForView(code, idGrade, Container, out parametersForView);

    [Serializable]
    public class Grade : BaseGrade
    {
        public FP Probability;
        public FP MinDistance;
        public CardType[] Types;

        public override FP[] ParametersArray => new[] {Probability * FP._100, MinDistance, FP._0,};

        public override void SetAdapter<TGradeAdapter>(TGradeAdapter adapter)
        {
            var currentAdapter = (GradeAdapter) (BaseGradeAdapter) adapter;
            Probability = FP.FromFloat_UNSAFE(currentAdapter.Probability);
            MinDistance = FP.FromFloat_UNSAFE(currentAdapter.MinDistance);
            Types = currentAdapter.Types;
        }
    }

    [Serializable]
    public class GradeAdapter : BaseGradeAdapter
    {
        public float Probability;
        public float MinDistance;
        public CardType[] Types;
    }
}

public struct FirstHitKillAfterChargePerkContainer
{
    public FP Probability;
    public FP MinDistance;
    public CardType[] Types;
}
}