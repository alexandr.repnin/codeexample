﻿using System;
using Photon.Deterministic;

namespace Quantum
{
[Serializable]
public class AcceleratedRetreatPerkSettings : EffectSettings
{
    public BaseContainer<BaseGradesWithKey<Grade>, Grade> Container;

    public bool TryGetGradeContainer(string code, int idGrade, out AcceleratedRetreatPerkContainer container)
    {
        container = default;
        
        if (!TryGetGrade(code, idGrade, Container, out var grade)) return false;
        
        container = new AcceleratedRetreatPerkContainer
        {
            MoveSpeedMultiplier = grade.MoveSpeedMultiplier,
        };

        return true;
    }
    
    public override void SetJSONData(string[] json) =>
        SetJSONData<BaseGradesWithKey<Grade>, Grade, GradeAdapter>(json, Container);
    
    public override int GetSettingsVariantIndex(string code) =>
        GetSettingsVariantIndex(code, Container);

    public override string GetSettingsCodeByVariantIndex(int variantIndex) =>
        GetSettingsCodeByVariantIndex(variantIndex, Container);

    public override bool TryGetParametersForView(string code, int idGrade, out ParameterForView[] parametersForView) =>
        TryGetParametersForView(code, idGrade, Container, out parametersForView);

    [Serializable]
    public class Grade : BaseGrade
    {
        public FP MoveSpeedMultiplier;

        public override FP[] ParametersArray => new[] {(MoveSpeedMultiplier - FP._1) * FP._100,};

        public override void SetAdapter<TGradeAdapter>(TGradeAdapter adapter)
        {
            var currentAdapter = (GradeAdapter) (BaseGradeAdapter) adapter;
            MoveSpeedMultiplier = FP.FromFloat_UNSAFE(currentAdapter.MoveSpeedMultiplier);
        }
    }

    [Serializable]
    public class GradeAdapter : BaseGradeAdapter
    {
        public float MoveSpeedMultiplier;
    }
}

public struct AcceleratedRetreatPerkContainer
{
    public FP MoveSpeedMultiplier;
}
}