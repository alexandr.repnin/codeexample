﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Photon.Deterministic;
using Quantum.Wargard.Extensions;

namespace Quantum
{
public abstract partial class EffectSettings
{
    public string Code;
    public AuraTarget Targets;
    
    public bool TryGetGrades<TBaseGradesWithKey, TGrade>(string code,
                                                         BaseContainer<TBaseGradesWithKey, TGrade> container,
                                                         out BaseGradesWithKey<TGrade> gradesWithKey)
        where TBaseGradesWithKey : BaseGradesWithKey<TGrade>
        where TGrade : BaseGrade
    {
        gradesWithKey = default;
        
        code.SplitComplexKey(out code, out var key);
        foreach (var gwk in container.GradesWithKeys)
        {
            if (gwk.Key != key) continue;
            
            gradesWithKey = gwk;
            break;
        }

        return gradesWithKey != null && !gradesWithKey.Equals(default);
    }

    protected bool TryGetGrade<TBaseGradesWithKey, TGrade>(string code,
                                                           int idGrade,
                                                           BaseContainer<TBaseGradesWithKey, TGrade> container,
                                                           out TGrade grade)
        where TBaseGradesWithKey : BaseGradesWithKey<TGrade>
        where TGrade : BaseGrade
    {
        grade = default;
        
        if (!TryGetGrades(code, container, out var gradesWithKey)) return false;

        grade = gradesWithKey.GetGrade(idGrade);
        return grade != null && !grade.Equals(default);
    }
    
    public bool TryGetParametersForView<TBaseGradesWithKey, TGrade>(string code,
                                                                    int idGrade,
                                                                    BaseContainer<TBaseGradesWithKey, TGrade> container,
                                                                    out ParameterForView[] parametersForView)
        where TBaseGradesWithKey : BaseGradesWithKey<TGrade>
        where TGrade : BaseGrade
    {
        parametersForView = Array.Empty<ParameterForView>();
        if (!TryGetGrades(code, container, out var gradesWithKey)) return false;
        
        parametersForView = gradesWithKey.GetParametersForView(idGrade);
        return true;
    }
    
    public virtual bool TryGetParametersForView(string code, int idGrade, out ParameterForView[] parametersForView)
    {
        parametersForView = Array.Empty<ParameterForView>();
        return true;
    }

    public void SetJSONData<TBaseGradesWithKey, TGrade, TGradeAdapter>(string[] json,
                                                                       BaseContainer<TBaseGradesWithKey, TGrade> container)
        where TBaseGradesWithKey : BaseGradesWithKey<TGrade>, new()
        where TGrade : BaseGrade, new()
        where TGradeAdapter : BaseGradeAdapter, new()
    {
        container.GradesWithKeys = json.Select(j =>
        {
            var adapter = JsonConvert.DeserializeObject<BaseGradesWithKeyAdapter<TGradeAdapter>>(j);
            var grades = adapter.Data.Grades.Select(g =>
            {
                var grade = new TGrade();
                grade.SetAdapter(g);
                return grade;
            }).ToArray();
            
            return new TBaseGradesWithKey
            {
                Key = adapter.Key,
                Grades = grades,
            };

        }).ToArray();
    }
    
    public int GetSettingsVariantIndex<TBaseGradesWithKey, TGrade>(string code, 
                                                                   BaseContainer<TBaseGradesWithKey, TGrade> container)
        where TBaseGradesWithKey : BaseGradesWithKey<TGrade>
        where TGrade : BaseGrade
    {
        code.SplitComplexKey(out code, out var key);
        for (var i = 0; i < container.GradesWithKeys.Length; i++)
        {
            if (container.GradesWithKeys[i].Key == key)
            {
                return i;
            }
        }
        
        return 0;
    }

    public string GetSettingsCodeByVariantIndex<TBaseGradesWithKey, TGrade>(int variantIndex, 
                                                                           BaseContainer<TBaseGradesWithKey, TGrade> container)
        where TBaseGradesWithKey : BaseGradesWithKey<TGrade>
        where TGrade : BaseGrade
    {
        return Code.CombineKey(variantIndex >= container.GradesWithKeys.Length
                                   ? container.GradesWithKeys[0].Key
                                   : container.GradesWithKeys[variantIndex].Key);
    }

    public abstract int GetSettingsVariantIndex(string code);
    
    public abstract string GetSettingsCodeByVariantIndex(int variantIndex);
    
    public virtual void GetPerkDependentSquads(string code, ref List<string> unitList) { }
    
    public virtual void SetJSONData(string[] json) { }
    
    [Serializable]
    public class BaseContainer<TBaseGradesWithKey, TGrade>
        where TBaseGradesWithKey : BaseGradesWithKey<TGrade>
        where TGrade : BaseGrade
    {
        public TBaseGradesWithKey[] GradesWithKeys;
    }

    [Serializable]
    public class BaseGradesWithKey<TGrade> where TGrade : BaseGrade
    {
        public string Key;
        public TGrade[] Grades;
        
        public TGrade GetGrade(int idGrade)
        {
            if (Grades == null || Grades == null || Grades.Length == 0)
                return null;

            idGrade = idGrade >= Grades.Length ? Grades.Length - 1 : idGrade < 0 ? 0 : idGrade;

            return Grades[idGrade];
        }
        
        protected void GetGrades(int idGrade, out TGrade grade, out TGrade nextGrade)
        {
            grade = GetGrade(idGrade);
            nextGrade = GetGrade(idGrade + 1);
        }
        
        public ParameterForView[] GetParametersForView(int idGrade)
        {
            GetGrades(idGrade, out var grade, out var nextGrade);

            var parameterForView = new ParameterForView[grade.ParametersArray.Length];
            if (parameterForView.Length <= 0) return parameterForView;
                
            for (var i = 0; i < parameterForView.Length; i++)
            {
                parameterForView[i] = new ParameterForView()
                {
                    Value = grade.ParametersArray[i],
                    CanGrade = grade.ParametersArray[i] != nextGrade.ParametersArray[i],
                    DeltaGrade = nextGrade.ParametersArray[i] - grade.ParametersArray[i],
                };
            }

            return parameterForView;
        }
    }

    [Serializable]
    public abstract class BaseGrade
    {
        public virtual FP[] ParametersArray => Array.Empty<FP>();

        public abstract void SetAdapter<TGradeAdapter>(TGradeAdapter adapter) where TGradeAdapter : BaseGradeAdapter;
    }
    
    [Serializable]
    public class BaseGradeWithRadius : BaseGrade
    {
        public FP Radius;

        public override FP[] ParametersArray => new[] {Radius,};

        public override void SetAdapter<TGradeAdapter>(TGradeAdapter adapter)
        {
            var currentAdapter = (BaseGradeWithRadiusAdapter) (BaseGradeAdapter) adapter;
            Radius = FP.FromFloat_UNSAFE(currentAdapter.Radius);
        }
    }
    
    [Serializable]
    public class BaseGradesWithKeyAdapter<TGradeAdapter> where TGradeAdapter : BaseGradeAdapter
    {
        public string Key;
        public DataAdapter<TGradeAdapter> Data;
    }
    
    [Serializable]
    public class DataAdapter<TGradeAdapter> where TGradeAdapter : BaseGradeAdapter
    {
        public TGradeAdapter[] Grades;
    }

    [Serializable]
    public class BaseGradeAdapter { }

    [Serializable]
    public class BaseGradeWithRadiusAdapter : BaseGradeAdapter
    {
        public float Radius;
    }
}

[Serializable]
public class ParameterForView
{
    public FP Value;
    public bool CanGrade;
    public FP DeltaGrade;
}
}