﻿using System;
using Photon.Deterministic;

namespace Quantum
{
[Serializable]
public class IgnoreSlowdownPerkSettings : EffectSettings
{
    public BaseContainer<BaseGradesWithKey<Grade>, Grade> Container;

    public bool TryGetGradeContainer(string code, int idGrade, out IgnoreSlowdownPerkContainer container)
    {
        container = default;
        
        if (!TryGetGrade(code, idGrade, Container, out var grade)) return false;
        
        container = new IgnoreSlowdownPerkContainer
        {
            Probability = grade.Probability,
            IsForMoveSpeed = grade.IsForMoveSpeed,
            IsForAttackSpeed = grade.IsForAttackSpeed,
        };

        return true;
    }
    
    public override void SetJSONData(string[] json) =>
        SetJSONData<BaseGradesWithKey<Grade>, Grade, GradeAdapter>(json, Container);
    
    public override int GetSettingsVariantIndex(string code) =>
        GetSettingsVariantIndex(code, Container);

    public override string GetSettingsCodeByVariantIndex(int variantIndex) =>
        GetSettingsCodeByVariantIndex(variantIndex, Container);

    public override bool TryGetParametersForView(string code, int idGrade, out ParameterForView[] parametersForView) =>
        TryGetParametersForView(code, idGrade, Container, out parametersForView);

    [Serializable]
    public class Grade : BaseGrade
    {
        public FP Probability;
        public bool IsForMoveSpeed;
        public bool IsForAttackSpeed;

        public override FP[] ParametersArray => new[] {Probability * FP._100, FP._0, FP._0,};

        public override void SetAdapter<TGradeAdapter>(TGradeAdapter adapter)
        {
            var currentAdapter = (GradeAdapter) (BaseGradeAdapter) adapter;
            Probability = FP.FromFloat_UNSAFE(currentAdapter.Probability);
            IsForMoveSpeed = currentAdapter.IsForMoveSpeed;
            IsForAttackSpeed = currentAdapter.IsForAttackSpeed;
        }
    }

    [Serializable]
    public class GradeAdapter : BaseGradeAdapter
    {
        public float Probability;
        public bool IsForMoveSpeed;
        public bool IsForAttackSpeed;
    }
}

public struct IgnoreSlowdownPerkContainer
{
    public FP Probability;
    public bool IsForMoveSpeed;
    public bool IsForAttackSpeed;
}
}