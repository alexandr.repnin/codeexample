﻿using Photon.Deterministic;
using Quantum.Inspector;

namespace Quantum
{
public partial class TutorialSettings
{
    public string Code;
    public bool IsDefault;
    public BattleMode BattleMode;
    public string MapCode;
    public bool DisableShuffleHand;
    public bool OnStartLockPlayerActions;
    public bool AddBattleReward;
    public bool HideWinPointsOnStart;
    public bool HidePlayerArmyBotControlOnStart;
    public bool ShowAdvices;
    public bool ShowPlayerArmyBotControlTips;
    public int DelayShowPlayerArmyBotControlTips;
    public FP BotManaVelocityMultiplier = FP._1;
    public FP BotDamageMultiplier = FP._1;
    public bool UsePlayerLobbyDeckAndRealCards;
    public DeckSlot[] PlayerSetup;
    public DeckSlot[] BotSetup;
    public AssetRefBotActionsPreset BotActionsPreset;

    [Space] public AssetRefBaseTutorialStep[] Steps;
    public int AutomaticExecuteSteps;

    public DeckSlot[] GetPlayerSetup() => PlayerSetup;
    
    public DeckSlot[] GetBotSetup() => BotSetup;

    public BaseTutorialStep GetStep(Frame f, int stepId)
    {
        return stepId < Steps.Length ? f.Assets.BaseTutorialStep(Steps[stepId]) : null;
    }
}
}