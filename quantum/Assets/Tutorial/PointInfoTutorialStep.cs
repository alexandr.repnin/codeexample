﻿using System;
using Photon.Deterministic;

namespace Quantum
{
[Serializable]
public class PointInfoTutorialStep : BaseTutorialStep
{
    public TutorialPointInfoType InfoType;
    public string CardCode;
    
    public override bool CanStart(Frame f)
    {
        switch (InfoType)
        {
            // can only be initialized via signal
            case TutorialPointInfoType.AllySquadDeath:
                return false;
            // same
            case TutorialPointInfoType.OpponentSquadDeath:
                return false;
            default:
                return true;
        }
    }

    public override bool CanStartThroughSignal(TutorialCanStartStepSignalType type, string cardCode) =>
        type == TutorialCanStartStepSignalType.AllySquadDeath && InfoType == TutorialPointInfoType.AllySquadDeath && CardCode.Equals(cardCode) ||
        type == TutorialCanStartStepSignalType.OpponentSquadDeath && InfoType == TutorialPointInfoType.OpponentSquadDeath && CardCode.Equals(cardCode);

    public override void Start(Frame f)
    {
        f.Events.ShowPointInfoTutorial(Code, InfoType, GetInfoPosition(f));
    }

    public override bool TryCompleteByTap() => true;

    private FPVector3 GetInfoPosition(Frame f)
    {
        var tutorial = f.GetSingleton<Tutorial>();
        
        switch (InfoType)
        {
            case TutorialPointInfoType.AllySquadDeath:
                return tutorial.DeadAllySquadLastPosition;
            case TutorialPointInfoType.OpponentSquadDeath:
                return tutorial.DeadOpponentSquadLastPosition;
            default:
                return FPVector3.Zero;
        }
    }
}
}