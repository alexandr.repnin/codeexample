﻿using System;
using Photon.Deterministic;
using Quantum.Wargard.Common;

namespace Quantum
{
[Serializable]
public class SquadOrderMovementTutorialStep : BaseTutorialStep
{
    public string CardCode;
    public int CardIndexOrder;
    public FPVector3 TargetPosition;

    public override bool CanStart(Frame f) => TryGetTargetSquadEntity(f, CardCode, CardIndexOrder, out var cardEntity);

    public override void Start(Frame f)
    {
        if (TryGetTargetSquadEntity(f, CardCode, CardIndexOrder, out var cardEntity))
        {
            f.Events.SetSquadOrderMovementTutorial(Code, cardEntity, TargetPosition);
        }
    }
    
    public override bool Execute(Frame f)
    {
        if (TryGetTargetSquadEntity(f, CardCode, CardIndexOrder, out var entityRef))
        {
            var orderType = OrderType.Movement;

            if (SquadHelper.TryGetNearSquadInRadius(f, OpponentPlayerIndex, TargetPosition, FP._4, out var targetEntity))
            {
                orderType = OrderType.Attack;
            }
            
            f.Signals.SquadSetOrder(TargetPlayerIndex, entityRef, (int) orderType, TargetPosition, targetEntity);
            
            return true;
        }
        
        return false;
    }

    public override bool TryCompleteByOrder(int playerIndex, string cardCode, OrderType orderType) =>
        TargetPlayerIndex == playerIndex && CardCode == cardCode 
                                         && (orderType == OrderType.Movement || orderType == OrderType.Attack);
}
}