﻿using System;
using Quantum.Wargard.Common;

namespace Quantum
{
[Serializable]
public class SquadInfoTutorialStep : BaseTutorialStep
{
    public string CardCode;
    public int CardIndexOrder;
    public int TeamId;
    public TutorialSquadInfoType InfoType;

    public override bool CanStart(Frame f) =>
        TroopHelper.TryGetSquadEntity(f, TeamId, CardCode, CardIndexOrder, out var cardEntity);

    public override void Start(Frame f)
    {
        if (TroopHelper.TryGetSquadEntity(f, TeamId, CardCode, CardIndexOrder, out var cardEntity))
        {
            f.Events.ShowSquadInfoTutorial(Code, cardEntity, TeamId, InfoType);
        }
    }

    public override bool TryCompleteByTap() => true;
}
}