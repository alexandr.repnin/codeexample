﻿using Photon.Deterministic;
using Quantum.Inspector;
using Quantum.Wargard.Common;

namespace Quantum
{
abstract partial class BaseTutorialStep
{
    public int SortPriority;
    public string Code;
    public bool IsDisabled;
    public bool IsPlayer;
    public FP DelayStart;
    public bool OnStartLockPlayerActions;
    public bool OnCompleteUnlockPlayerActions;
    public bool SaveAsCompleted;
    [Space] public TutorialDescriptionType DescriptionType = TutorialDescriptionType.Empty;

    protected int TargetPlayerIndex => IsPlayer ? 0 : 1;
    protected int OpponentPlayerIndex => IsPlayer ? 1 : 0;
    
    public abstract bool CanStart(Frame f);

    public virtual bool CanStartThroughSignal(TutorialCanStartStepSignalType type, string cardCode) => false;

    public abstract void Start(Frame f);

    public virtual bool Execute(Frame f) => false;

    public virtual bool TryCompleteByApplyCard(int playerIndex, string cardCode) => false;

    public virtual bool TryCompleteByTap() => false;
    
    public virtual bool TryCompleteByOrder(int playerIndex, string cardCode, OrderType orderType) => false;

    public virtual bool TryCompleteBySetPlayerArmyBotControl(int playerIndex, bool status) => false;

    public void OnStart(Frame f)
    {
        if (DescriptionType == TutorialDescriptionType.Empty) return;
        
        f.Events.ShowDescriptionTutorial(Code, DescriptionType);
    }
    
    protected bool TryGetTargetSquadEntity(Frame f, string code, int indexOrder, out EntityRef entityRef) =>
        TroopHelper.TryGetSquadEntity(f, TargetPlayerIndex, code, indexOrder, out entityRef);
    
    protected bool TryGetOpponentTargetSquadEntity(Frame f, string code, int indexOrder, out EntityRef entityRef) =>
        TroopHelper.TryGetSquadEntity(f, OpponentPlayerIndex, code, indexOrder, out entityRef);
}
}