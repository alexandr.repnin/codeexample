﻿using System;
using Photon.Deterministic;
using Quantum.Wargard.Common;

namespace Quantum
{
[Serializable]
public class ApplyCardTutorialStep : BaseTutorialStep
{
    public string CardCode;
    public FPVector3 TargetPosition;

    public override bool CanStart(Frame f) => ArmyHelper.CanApplyCard(f, TargetPlayerIndex, CardCode);

    public override void Start(Frame f)
    {
        if (ArmyHelper.TryGetFirstSlotIDByCardCode(f, TargetPlayerIndex, CardCode, out var slotID))
        {
            f.Events.ApplyCardTutorial(Code, slotID, TargetPosition, false);
        }
    }
    
    public override bool Execute(Frame f)
    {
        if (f.Assets.CardsGroupAsset(f.RuntimeConfig.Cards).TryGetCardSpec(f, CardCode, out var cardSettings))
        {
            var targetEntity = EntityRef.None;
            
            if (cardSettings.IsSquad && 
                SquadHelper.TryGetNearSquadInRadius(f, OpponentPlayerIndex, TargetPosition, FP._4, out targetEntity)) { }

            if (ArmyHelper.TryGetFirstSlotIDByCardCode(f, TargetPlayerIndex, CardCode, out var slotID))
            {
                f.Signals.ApplyCard(TargetPlayerIndex, slotID, true, TargetPosition, targetEntity);
                return true;
            }
        }

        return false;
    }

    public override bool TryCompleteByApplyCard(int playerIndex, string cardCode) =>
        TargetPlayerIndex == playerIndex && CardCode == cardCode;
}
}