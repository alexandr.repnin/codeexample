﻿using System;

namespace Quantum
{
[Serializable]
public unsafe class SetPlayerArmyBotControlTutorialStep : BaseTutorialStep
{
    public override bool CanStart(Frame f) => f.Global -> BattleMode == BattleMode.AutoBattle;

    public override void Start(Frame f) => f.Events.SetPlayerArmyBotControlTutorial(Code);

    public override bool Execute(Frame f)
    {
        f.Signals.ActivateArmyBotControl(0, true);
        return false;
    }

    public override bool TryCompleteBySetPlayerArmyBotControl(int playerIndex, bool status) =>
        playerIndex == 0 && status;
}
}