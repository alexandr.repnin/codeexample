﻿using System;
using Photon.Deterministic;
using Quantum.Inspector;
using Quantum.Wargard.Common;

namespace Quantum
{
[Serializable]
public class DescriptionTutorialStep : BaseTutorialStep
{
    [Space] public TutorialDescriptionConditionType ConditionType;
    public SquadAtTargetPointConditionSettings SquadAtTargetPointCondition;

    public override bool CanStart(Frame f)
    {
        return ConditionType switch
        {
            TutorialDescriptionConditionType.Empty => true,
            TutorialDescriptionConditionType.SquadAtTargetPoint => SquadAtTargetPointCondition.IsSatisfyCondition(f),
            _ => true,
        };
    }

    public override void Start(Frame f) { }

    public override bool TryCompleteByTap() => true;
    
    [Serializable]
    public unsafe struct SquadAtTargetPointConditionSettings
    {
        public string Code;
        public int IndexOrder;
        public int TeamId;

        public bool IsSatisfyCondition(Frame f)
        {
            if (!TroopHelper.TryGetSquadEntity(f, TeamId, Code, IndexOrder, out var entityRef)) return false;

            var position = f.Unsafe.GetPointer<Transform3D>(entityRef) -> Position.XZ;
            var targetPosition = f.Unsafe.TryGetPointer<SquadMotor>(entityRef, out var squadMotor)
                ? squadMotor -> TargetPosition.XZ
                : position;
            
            return FPVector2.Distance(position, targetPosition) < FP._1;
        }
    }
}
}