﻿using System;
using Photon.Deterministic;

namespace Quantum
{
[Serializable]
public class SquadOrderAttackTutorialStep : BaseTutorialStep
{
    public string CardCode;
    public int CardIndexOrder;
    public string TargetCardCode;
    public int TargetCardIndexOrder;

    public override bool CanStart(Frame f) =>
        TryGetTargetSquadEntity(f, CardCode, CardIndexOrder, out var cardEntity) &&
        TryGetOpponentTargetSquadEntity(f, TargetCardCode, TargetCardIndexOrder, out var targetCardEntity);

    public override void Start(Frame f)
    {
        if (TryGetTargetSquadEntity(f, CardCode, CardIndexOrder, out var cardEntity) &&
            TryGetOpponentTargetSquadEntity(f, TargetCardCode, TargetCardIndexOrder, out var targetCardEntity))
        {
            f.Events.SetSquadOrderAttackTutorial(Code, cardEntity, targetCardEntity);
        }
    }
    
    public override bool Execute(Frame f)
    {
        if (TryGetTargetSquadEntity(f, CardCode, CardIndexOrder, out var targetE) && 
            TryGetOpponentTargetSquadEntity(f, TargetCardCode, TargetCardIndexOrder, out var opponentTargetE))
        {
            f.Signals.SquadSetOrder(TargetPlayerIndex, targetE, (int) OrderType.Attack, FPVector3.Zero,
                                    opponentTargetE);
            
            return true;
        }
        
        return false;
    }
    
    public override bool TryCompleteByOrder(int playerIndex, string cardCode, OrderType orderType) =>
        TargetPlayerIndex == playerIndex && CardCode == cardCode && orderType == OrderType.Attack;
}
}