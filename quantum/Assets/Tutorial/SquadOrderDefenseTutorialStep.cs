﻿using System;
using Photon.Deterministic;

namespace Quantum
{
[Serializable]
public class SquadOrderDefenseTutorialStep : BaseTutorialStep
{
    public string CardCode;
    public int CardIndexOrder;
    
    public override bool CanStart(Frame f) => TryGetTargetSquadEntity(f, CardCode, CardIndexOrder, out var cardEntity);

    public override void Start(Frame f)
    {
        if (TryGetTargetSquadEntity(f, CardCode, CardIndexOrder, out var cardEntity))
        {
            f.Events.SetSquadOrderDefenseTutorial(Code, cardEntity);
        }
    }
    
    public override bool Execute(Frame f)
    {
        if (!TryGetTargetSquadEntity(f, CardCode, CardIndexOrder, out var entityRef)) return false;
        
        f.Signals.SquadSetOrder(TargetPlayerIndex, entityRef, (int) OrderType.Defense, FPVector3.Zero, 
                                EntityRef.None);
            
        return true;
    }
    
    public override bool TryCompleteByOrder(int playerIndex, string cardCode, OrderType orderType) =>
        TargetPlayerIndex == playerIndex && CardCode == cardCode && orderType == OrderType.Defense;
}
}