﻿using System;
using Photon.Deterministic;
using Quantum.Wargard.Common;

namespace Quantum
{
[Serializable]
public unsafe class FreeApplyCardTutorialStep : BaseTutorialStep
{
    public override bool CanStart(Frame f) => ArmyHelper.CanApplyAnyCard(f, TargetPlayerIndex);

    public override void Start(Frame f)
    {
        if (ArmyHelper.TryGetRandomSlotID(f, TargetPlayerIndex, out var slotID, out var code))
        {
            var targetPosition = FPVector3.Back * 15;

            if (SquadHelper.TryGetRandomSquad(f, OpponentPlayerIndex, out var targetSquadEntity))
            {
                targetPosition = f.Unsafe.GetPointer<Troop>(targetSquadEntity) -> Position;
            }
            
            f.Events.ApplyCardTutorial(Code, slotID, targetPosition, true);
        }
    }
    
    public override bool Execute(Frame f)
    {
        if (ArmyHelper.TryGetRandomSlotID(f, TargetPlayerIndex, out var slotID, out var code) && 
            f.Assets.CardsGroupAsset(f.RuntimeConfig.Cards).TryGetCardSpec(f, code, out var cardSettings))
        {
            var targetEntity = EntityRef.None;
            var targetPosition = FPVector3.Back * 15;

            if (SquadHelper.TryGetRandomSquad(f, OpponentPlayerIndex, out var targetSquadEntity))
            {
                targetPosition = f.Unsafe.GetPointer<Troop>(targetSquadEntity) -> Position;
            }

            if (cardSettings.IsSquad && 
                SquadHelper.TryGetNearSquadInRadius(f, OpponentPlayerIndex, targetPosition, FP._4, out targetEntity)) { }

            f.Signals.ApplyCard(TargetPlayerIndex, slotID, true, targetPosition, targetEntity);
            
            return true;
        }

        return false;
    }

    public override bool TryCompleteByApplyCard(int playerIndex, string cardCode) => TargetPlayerIndex == playerIndex;
}
}