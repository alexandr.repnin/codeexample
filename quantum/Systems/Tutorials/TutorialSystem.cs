﻿using Photon.Deterministic;
using Quantum.Wargard.Other.Common;

namespace Quantum.Wargard.Systems.Tutorials
{
public unsafe class TutorialSystem : SystemMainThreadFilter<TutorialSystem.Filter>, ISignalStartTutorial, ISignalOnCardApplied, 
                                     ISignalTapTutorialStep, ISignalOnSetSquadOrder, ISignalOnSquadDead, ISignalActivateArmyBotControl
{
    public struct Filter
    {
        public EntityRef Entity;
        public Tutorial* Tutorial;
    }

    public override void Update(Frame f, ref Filter filter)
    {
        if (f.Global -> BattleController.State != BattleState.Running) return;

        if (filter.Tutorial -> StepIsActive) return;

        if (!TryGetCurrentStep(f, out var step)) return;

        var currentTime = Time.GetCurrentTime(f);
        if (currentTime - filter.Tutorial->TimeLastStep < step.DelayStart) return;
        
        if (step.CanStart(f))
        {
            StartStep(f, filter.Tutorial, step);
        }
    }

    public void StartTutorial(Frame f)
    {
        var prototypeRef = f.FindAsset<PrototypeAssetRefs>(f.RuntimeConfig.PrototypeAssetRefs.Id).Tutorial;
        var entity = f.Create(prototypeRef);

        var tutorialComponent = f.Unsafe.GetPointer<Tutorial>(entity);
        tutorialComponent -> StepId = 0;
        tutorialComponent -> StepIsActive = false;
        tutorialComponent -> TimeLastStep = Time.GetCurrentTime(f);

        SkipDisabledSteps(f, tutorialComponent);

        if (IsLastStep(f))
        {
            FinishTutorial(f);
        }
    }

    private void FinishTutorial(Frame f)
    {
        f.Signals.InitializeBotAgentAdjustPlayerLevel(1);
        
        f.SystemDisable<TutorialSystem>();

        f.Destroy(f.GetSingletonEntityRef<Tutorial>());
    }

    private void StartStep(Frame f, Tutorial* tutorial, BaseTutorialStep step)
    {
        tutorial -> StepIsActive = true;

        if (step.OnStartLockPlayerActions)
        {
            f.Events.LockPlayerActionsTutorial(true);
        }
        
        if (step.SaveAsCompleted)
        {
            
        }

        var isAutomaticExecute = IsAutomaticExecuteCurrentStep(f);

        if (step.IsPlayer && !isAutomaticExecute)
        {
            f.Signals.PauseBattle();
            
            step.Start(f);
            step.OnStart(f);
        }
        else
        {
            var result = step.Execute(f);

            if (!result)
            {
                CompleteStep(f, step, isAutomaticExecute);
            }
        }
    }

    private void CompleteStep(Frame f, BaseTutorialStep step, bool isAutomaticExecute)
    {
        if (step.IsPlayer && !isAutomaticExecute)
        {
            f.Events.CompleteStepTutorial(step.Code);
            
            f.Signals.ResumeBattle();
        }

        var tutorialComponent = f.Unsafe.GetPointerSingleton<Tutorial>();
        tutorialComponent -> StepId++;
        tutorialComponent -> StepIsActive = false;
        tutorialComponent -> TimeLastStep = Time.GetCurrentTime(f);
        
        if (step.OnCompleteUnlockPlayerActions)
        {
            f.Events.LockPlayerActionsTutorial(false);
        }

        SkipDisabledSteps(f, tutorialComponent);

        if (IsLastStep(f))
        {
            FinishTutorial(f);
        }
    }

    private void SkipDisabledSteps(Frame f, Tutorial* tutorial)
    {
        while (TryGetCurrentStep(f, out var step) && step.IsDisabled)
        {
            tutorial -> StepId++;
        }
    }

    public void OnCardApplied(Frame f, int playerIndex, string code)
    {
        if (!TryGetCurrentStep(f, out var step)) return;

        if (step.TryCompleteByApplyCard(playerIndex, code))
        {
            CompleteStep(f, step, false);
        }
    }
    
    public void TapTutorialStep(Frame f)
    {
        if (!TryGetCurrentStep(f, out var step)) return;

        if (step.TryCompleteByTap())
        {
            CompleteStep(f, step, false);
        }
    }

    public void OnSetSquadOrder(Frame f, EntityRef entityRef, OrderType orderType, EntityRef targetEntityRef)
    {
        if (!TryGetCurrentStep(f, out var step)) return;
        
        if (!f.Unsafe.TryGetPointer<Team>(entityRef, out var team) ||
            !f.Unsafe.TryGetPointer<Troop>(entityRef, out var troop)) return;
        
        var settings = f.Assets.CardSettings(troop -> Settings);
        
        if (step.TryCompleteByOrder(team -> ID, settings.Code, orderType))
        {
            CompleteStep(f, step, false);
        }
    }

    public void ActivateArmyBotControl(Frame f, int playerIndex, QBoolean status)
    {
        if (!TryGetCurrentStep(f, out var step)) return;
        
        if (step.TryCompleteBySetPlayerArmyBotControl(playerIndex, status))
        {
            CompleteStep(f, step, false);
        }
    }

    public void OnSquadDead(Frame f, EntityRef squadEntity)
    {
        var troopComponent = f.Unsafe.GetPointer<Troop>(squadEntity);
        var code = f.Assets.CardSettings(troopComponent -> Settings).Code;
        var teamId = f.Unsafe.GetPointer<Team>(squadEntity) -> ID;
        var position = f.Unsafe.GetPointer<Troop>(squadEntity) -> Position;

        if (!TryGetCurrentStep(f, out var step)) return;

        var signalType = teamId == 0
            ? TutorialCanStartStepSignalType.AllySquadDeath
            : TutorialCanStartStepSignalType.OpponentSquadDeath;
        
        if (step.CanStartThroughSignal(signalType, code))
        {
            var tutorial = f.Unsafe.GetPointerSingleton<Tutorial>();
            if (teamId == 0)
            {
                tutorial -> DeadAllySquadLastPosition = position;
            }
            else
            {
                tutorial -> DeadOpponentSquadLastPosition = position;
            }

            StartStep(f, tutorial, step);
        }
    }

    private bool TryGetCurrentStep(Frame f, out BaseTutorialStep step)
    {
        step = null;
        
        var stepId = f.GetSingleton<Tutorial>().StepId;
        var tutorialSettings = f.Assets.TutorialSettings(f.RuntimeConfig.Tutorial);
        step = tutorialSettings.GetStep(f, stepId);

        return step != null;
    }

    private bool IsAutomaticExecuteCurrentStep(Frame f)
    {
        var stepId = f.GetSingleton<Tutorial>().StepId;
        var tutorialSettings = f.Assets.TutorialSettings(f.RuntimeConfig.Tutorial);

        return stepId <= tutorialSettings.AutomaticExecuteSteps;
    }

    private bool IsLastStep(Frame f)
    {
        var stepId = f.GetSingleton<Tutorial>().StepId;
        var tutorialSettings = f.Assets.TutorialSettings(f.RuntimeConfig.Tutorial);

        return stepId >= tutorialSettings.Steps.Length;
    }
}
}