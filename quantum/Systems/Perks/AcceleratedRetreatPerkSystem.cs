﻿using Photon.Deterministic;
using Quantum.Wargard.Common;
using Quantum.Wargard.Systems.Squads;

namespace Quantum.Wargard.Systems.Perks
{
public unsafe class AcceleratedRetreatPerkSystem : SystemMainThreadFilter<AcceleratedRetreatPerkSystem.Filter>, IPerkSystem, ISignalApplyAcceleratedRetreatPerk
{
    public struct Filter
    {
        public EntityRef Entity;
        public AcceleratedRetreatPerk* Perk;
        public SquadMotor* SquadMotor;
    }

    private readonly FP RetreatAngle = FP.FromFloat_UNSAFE(45);

    public override void Update(Frame f, ref Filter filter)
    {
        var isHaveMovementOrder =
            SquadBlackboardSetterSystem.TryGetMovementOrder(f, filter.Entity, out var orderTargetPosition);
        
        if (filter.Perk -> IsApplied)
        {
            if (isHaveMovementOrder)
            {
                if (orderTargetPosition != filter.Perk -> OrderTargetPosition)
                {
                    if (IsOrderRetreat(f, filter.Entity, orderTargetPosition))
                    {
                        filter.Perk -> OrderTargetPosition = orderTargetPosition;
                    }
                    else
                    {
                        StartStop(f, filter, false);
                    }
                }
            }
            else
            {
                StartStop(f, filter, false);
            }
        }
        else if (isHaveMovementOrder && IsOrderRetreat(f, filter.Entity, orderTargetPosition))
        {
            filter.Perk -> OrderTargetPosition = orderTargetPosition;
            StartStop(f, filter, true);
        }
    }
    
    public void ApplyAcceleratedRetreatPerk(Frame f, EntityRef squadEntity, AcceleratedRetreatPerkContainer container)
    {
        f.Add(squadEntity, new AcceleratedRetreatPerk
        {
            MoveSpeedMultiplier = container.MoveSpeedMultiplier,
        });
    }

    private bool IsOrderRetreat(Frame f, EntityRef e, FPVector3 targetPosition)
    {
        var team = f.Unsafe.GetPointer<Team>(e);
        var backDirection = team -> ID == 0 ? FPVector3.Back : FPVector3.Forward;
        
        var troop = f.Unsafe.GetPointer<Troop>(e);
        var targetPointDirection = FPVector3.Normalize(targetPosition - troop -> Position);

        var angle = FPMath.Abs(FPVector3.Angle(targetPointDirection, backDirection));
        
        return angle <= RetreatAngle;
    }

    private void StartStop(Frame f, Filter filter, bool status)
    {
        if (filter.Perk -> IsApplied == status) return;

        MultipliersHelper.ApplyMoveSpeedMultiplierToTroop(f, filter.Entity, EntityRef.None,
                                                          filter.Perk -> MoveSpeedMultiplier, !status);
        filter.Perk -> IsApplied = status;
    }
}
}