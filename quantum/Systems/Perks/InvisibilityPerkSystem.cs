﻿using System;
using Photon.Deterministic;
using Quantum.Wargard.Common;
using Quantum.Wargard.Other.Common;

namespace Quantum.Wargard.Systems.Perks
{
public unsafe class InvisibilityPerkSystem : SystemMainThreadFilter<InvisibilityPerkSystem.Filter>, IPerkSystem, 
                                             ISignalApplyInvisibilityPerk, ISignalOnTakeDamage
{
    public struct Filter
    {
        public EntityRef Entity;
        public Troop* Troop;
        public Squad* Squad;
        public InvisibilityPerk* Perk;
    }

    private readonly FP deltaCheckInvisibility = FP._0_20;

    public override void Update(Frame f, ref Filter filter)
    {
        var currentTime = Time.GetCurrentTime(f);

        if (currentTime - filter.Perk -> TimeLastCheckInvisibility > deltaCheckInvisibility)
        {
            var isOnFlag = SquadHelper.IsAtLeastOneUnitOfSquadOnFlag(f, filter.Entity);
            
            if (!filter.Troop -> IsVisible && isOnFlag)
            {
                SetVisible(f, filter.Entity, true);
            }

            if (filter.Troop -> IsVisible)
            {
                if (SquadHelper.IsSquadInDiscomfortRadiusOfAnyOpponentSquad(f, filter.Entity) || isOnFlag)
                {
                    filter.Perk -> TimerInvisibility = filter.Perk -> Delay;
                }
                else
                {
                    filter.Perk -> TimerInvisibility -= deltaCheckInvisibility;

                    if (filter.Perk -> TimerInvisibility <= FP._0)
                    {
                        SetVisible(f, filter.Entity, false);
                    }
                }
            }
            
            filter.Perk -> TimeLastCheckInvisibility = currentTime;
        }
    }

    public void ApplyInvisibilityPerk(Frame f, EntityRef squadEntity, InvisibilityPerkContainer container)
    {
        var perkComponent = new InvisibilityPerk()
        {
            Delay = container.Delay,
            TimerInvisibility = container.Delay,
        };
        
        f.Add(squadEntity, perkComponent);
    }

    public void OnTakeDamage(Frame f, EntityRef entityRef, EntityRef attackerEntityRef)
    {
        SetVisible(f, entityRef, true);
        SetVisible(f, attackerEntityRef, true);
    }

    private void SetVisible(Frame f, EntityRef entityRef, bool status)
    {
        if (!f.Exists(entityRef) ||
            !f.Unsafe.TryGetPointer<Troop>(entityRef, out var troop) ||
            !f.Unsafe.TryGetPointer<InvisibilityPerk>(entityRef, out var perk))
        {
            return;
        }

        if (troop -> IsVisible != status)
        {
            f.Events.ChangeIsInvisible(entityRef);
        }
        
        troop -> IsVisible = status;
        perk -> TimerInvisibility = perk -> Delay;
    }
}
}