﻿using System.Linq;
using Photon.Deterministic;
using Quantum.Wargard.Common;
using Quantum.Wargard.Extensions;

namespace Quantum.Wargard.Systems.Perks
{
public unsafe class FirstHitKillAfterChargePerkSystem : SystemMainThreadFilter<FirstHitKillAfterChargePerkSystem.Filter>, IPerkSystem,
                                                        ISignalApplyFirstHitKillAfterChargePerk, ISignalOnSetSquadOrder
{
    public struct Filter
    {
        public EntityRef Entity;
        public FirstHitKillAfterChargePerk* Perk;
        public ChargePerk* ChargePerk;
        public Squad* Squad;
        public Troop* Troop;
    }

    public override void Update(Frame f, ref Filter filter)
    {
        if (filter.ChargePerk -> IsApplied && !filter.Perk -> IsCharged && IsCorrectDistance(f, filter))
        {
            Start(f, filter);
        }
        else if (!filter.ChargePerk -> IsApplied && filter.Perk -> IsCharged)
        {
            Stop(f, filter);
        }
    }

    public void ApplyFirstHitKillAfterChargePerk(Frame f,
                                                 EntityRef squadEntity,
                                                 FirstHitKillAfterChargePerkContainer container)
    {
        var chargePerk = new FirstHitKillAfterChargePerk
        {
            Probability = container.Probability,
            MinDistance = container.MinDistance,
            Types = QListExtensions.AllocateList(f, container.Types.ToList()),
        };

        f.Add(squadEntity, chargePerk);
    }
    
    private bool IsCorrectDistance(Frame f, Filter filter)
    {
        if (!f.Exists(filter.Squad -> LastOrderTargetAttack))
        {
            return false;
        }

        var targetPosition =
            f.Unsafe.GetPointer<Troop>(filter.Squad -> LastOrderTargetAttack) -> Position;
        return FPVector3.Distance(filter.Troop -> Position, targetPosition) >= filter.Perk -> MinDistance;
    }

    private void Start(Frame f, Filter filter)
    {
        filter.Perk -> TargetAttack = filter.Squad -> LastOrderTargetAttack;
        filter.Perk -> IsCharged = true;
        filter.Perk -> IsApplied = false;
    }
    
    private void Stop(Frame f, Filter filter)
    {
        if (!filter.ChargePerk -> IsStopByOtherOrder && f.Exists(filter.Perk -> TargetAttack) &&
            filter.Squad -> LastOrderTargetAttack == filter.Perk -> TargetAttack)
        {
            var firstStrikeAfterChargeMultiplier = new FirstHitKillUnit
            {
                Probability = filter.Perk -> Probability,
                Types = QListExtensions.AllocateList(f, f.ResolveList(filter.Perk -> Types).ToList()),
            };
            PerkHelper.AddPerkToAllUnitsOfSquad(f, filter.Entity, firstStrikeAfterChargeMultiplier);
            
            filter.Perk -> IsApplied = true;
        }

        filter.Perk -> IsCharged = false;
    }

    public void OnSetSquadOrder(Frame f, EntityRef entityRef, OrderType orderType, EntityRef targetEntityRef)
    {
        if (!f.Unsafe.TryGetPointer<FirstHitKillAfterChargePerk>(entityRef, out var perk)) return;

        if (perk -> IsApplied && targetEntityRef != perk -> TargetAttack)
        {
            PerkHelper.RemovePerkFromAllUnitsOfSquad<FirstHitKillUnit>(f, entityRef);
            
            perk -> IsCharged = false;
            perk -> IsApplied = false;
        }
    }
}
}