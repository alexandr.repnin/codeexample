﻿using System.Linq;
using Photon.Deterministic;
using Quantum.Wargard.Extensions;

namespace Quantum.Wargard.Systems.Perks
{
public unsafe class IgnoreSlowdownPerkSystem : SystemSignalsOnly, IPerkSystem, ISignalApplyIgnoreSlowdownPerk
{
    public enum SlowdownType
    {
        Empty,
        MoveSpeed,
        AttackSpeed,
    }
    
    public void ApplyIgnoreSlowdownPerk(Frame f, EntityRef squadEntity, IgnoreSlowdownPerkContainer container)
    {
        f.Add(squadEntity, new IgnoreSlowdownPerk
        {
            Probability = container.Probability,
            IsForMoveSpeed = container.IsForMoveSpeed,
            IsForAttackSpeed = container.IsForAttackSpeed,
            IgnoredOwners = f.AllocateList<EntityRef>(),
        });
    }

    public static bool IsIgnoreStun(Frame f, EntityRef target)
    {
        if (!f.Unsafe.TryGetPointer<IgnoreSlowdownPerk>(target, out var perk)) return false;
        
        var isRandomized = f.Global -> RngSession.Next(FP._0, FP._1) <= perk -> Probability;
        
        return isRandomized;
    }

    public static bool TryProceed(Frame f, EntityRef target, EntityRef owner, FP multiplier, SlowdownType type)
    {
        if (!IsSuitableForProceed(f, target, owner, multiplier, type, out var perk)) return false;

        var ignoredOwners = f.ResolveList(perk -> IgnoredOwners).ToList();
        var isRandomized = f.Global -> RngSession.Next(FP._0, FP._1) <= perk -> Probability;

        if (isRandomized && !ignoredOwners.Contains(owner))
        {
            ignoredOwners.Add(owner);
            perk -> IgnoredOwners = QListExtensions.AllocateList(f, ignoredOwners);
        }

        return ignoredOwners.Contains(owner);
    }

    public static bool TryRemoveProceed(Frame f, EntityRef target, EntityRef owner, FP multiplier, SlowdownType type)
    {
        if (!IsSuitableForProceed(f, target, owner, multiplier, type, out var perk)) return false;
        
        var ignoredOwners = f.ResolveList(perk -> IgnoredOwners).ToList();

        if (ignoredOwners.Contains(owner))
        {
            ignoredOwners.Remove(owner);
            perk -> IgnoredOwners = QListExtensions.AllocateList(f, ignoredOwners);
            
            return true;
        }

        return false;
    }

    private static bool IsSuitableForProceed(Frame f, EntityRef target, EntityRef owner, FP multiplier, SlowdownType type, out IgnoreSlowdownPerk* perk)
    {
        perk = default;
        
        if (multiplier >= FP._1 || !f.Exists(owner)) return false;
        
        if (!f.Unsafe.TryGetPointer(target, out perk)) return false;

        var perkSlowdownType = GetPerkSlowdownType(perk);

        return type == perkSlowdownType;
    }

    private static SlowdownType GetPerkSlowdownType(IgnoreSlowdownPerk* perk)
    {
        if (perk -> IsForMoveSpeed)
        {
            return SlowdownType.MoveSpeed;
        }
        
        if (perk -> IsForAttackSpeed)
        {
            return SlowdownType.AttackSpeed;
        }
        
        return SlowdownType.Empty;
    }
}
}