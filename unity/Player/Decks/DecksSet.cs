﻿using System.Linq;
using Wargard.SharedLogic.MetaModels.Cards.Decks;
using Wargard.SharedLogic.Source.GameSettings.DeckBuildingsSettings;

namespace Wargard.Player.Decks
{
public class DecksSet
{
    public readonly Deck[] decks;
    private readonly DeckSetModel model;

    public int SelectedDeck => model.SelectedDeck;
    public Deck CurrentDeck => decks[SelectedDeck];
    public Deck BotDeck => decks.Last();
    public DeckBuildingSettings DeckBuilding { get; }

    public DecksSet(Deck[] decks,
                    DeckSetModel model,
                    DeckBuildingSettings settings)
    {
        this.decks = decks;
        this.model = model;
        DeckBuilding = settings;
    }
}
}
