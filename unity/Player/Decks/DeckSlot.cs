using Wargard.Client.Network;
using Wargard.Common;
using Wargard.SharedLogic.MetaModels.Cards.Decks;
using Zenject;
using Card = Wargard.Player.Cards.Card;

namespace Wargard.Player.Decks
{
public class DeckSlot
{
    public readonly int id;
    private readonly DeckSlotModel model;
    public readonly int maxAmount;
    private readonly CardsDirector cardsDirector;
    private readonly PlayerDecks playerDecks;

    public int Amount => model.Amount;
    public Card Card => cardsDirector.GetCard(model.CardCode);
    public bool IsEmpty => Card == null;
    public bool IsFullyFilled => model.Amount >= maxAmount;
    public string Code => model.CardCode;
    public int IdGrade => !IsEmpty ? Card.Grade : -1;
    public int Cost => !IsEmpty ? Card.Mana * Amount : 0;
    
    public DeckSlot(int id,
                    DeckSlotModel model,
                    int copiesInSlot,
                    CardsDirector cardsDirector,
                    PlayerDecks playerDecks,
                    SignalBus signalBus)
    {
        this.id = id;
        this.model = model;
        maxAmount = copiesInSlot;
        this.cardsDirector = cardsDirector;
        this.playerDecks = playerDecks;

        signalBus.Subscribe<GameRestartedSignal>(OnGameRestarted);
        
        SubscribeModelActions();
    }
    
    private void OnGameRestarted(GameRestartedSignal signal)
    {
        UnsubscribeModelActions();
    }

    private void SubscribeModelActions()
    {
        model.OnCardChanged += CardChanged;
        model.OnCopiesChanged += CopiesChanged;
    }
    
    private void UnsubscribeModelActions()
    {
        model.OnCardChanged -= CardChanged;
        model.OnCopiesChanged -= CopiesChanged;
    }
    
    private void CardChanged() => playerDecks.SetSlotCardChanged(this);

    private void CopiesChanged() => playerDecks.SetSlotCopiesChanged(this);
}
}