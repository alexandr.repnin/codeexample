﻿using System.Collections.Generic;
using System.Linq;
using Wargard.Client.Network;
using Wargard.SharedLogic.MetaModels.Cards.Decks;
using Zenject;

namespace Wargard.Player.Decks
{
public class Deck
{
    public readonly int id;
    public readonly DeckSlot[] slots;
    private readonly DeckModel model;
    private readonly PlayerDecks playerDecks;
    
    public bool IsFull => slots.FirstOrDefault(s => s.IsEmpty) == null;
    public bool IsEmpty => slots.FirstOrDefault(s => !s.IsEmpty) == null;

    public Deck(int id, DeckSlot[] slots, DeckModel model, PlayerDecks playerDecks, SignalBus signalBus)
    {
        this.id = id;
        this.slots = slots;
        this.model = model;
        this.playerDecks = playerDecks;

        signalBus.Subscribe<GameRestartedSignal>(OnGameRestarted);
        
        SubscribeModelActions();
    }
    
    private void OnGameRestarted(GameRestartedSignal signal)
    {
        UnsubscribeModelActions();
    }
    
    private void SubscribeModelActions()
    {
        model.OnChanged += SetChanged;
    }
    
    private void UnsubscribeModelActions()
    {
        model.OnChanged -= SetChanged;
    }
    
    private void SetChanged() => playerDecks.SetDeckChanged(this);
    
    public bool HasCardWithCode(string cardCode, out int slotIndex)
    {
        slotIndex = -1;
        var targetSlot = slots.FirstOrDefault(s => s.Code == cardCode);
        if (targetSlot != null)
        {
            slotIndex = targetSlot.id;
            return true;
        }
        
        return false;
    } 
    
    public float GetAverageCost()
    {
        var amount = GetAmountCards();
        if (amount > 0)
            return (float) GetCost() / amount;

        return 0F;
    }

    public int GetCost() => slots.Sum(ds => ds.Cost);

    public int GetLevel() => slots.Sum(slot => (slot.IdGrade + 1) * slot.Amount);

    public int GetAmountCards() => slots.Sum(ds => ds.Amount);

    public int GetAmountCards(string code) => slots.Where(deckSlot => deckSlot.Code == code).Sum(deckSlot => deckSlot.Amount);

    public bool CanStartBattle(int minCards, int minCost) => GetAmountCards() >= minCards && GetCost() >= minCost;

    public string[] ToArray() =>
        slots.Where(s => !string.IsNullOrEmpty(s.Code))
             .Select(s => $"{s.Code}")
             .ToArray();

    public string[] ArmyFullSetup()
    {
        var armySetup = new List<string>();

        foreach (var slot in slots)
        {
            for (int i = 0; i < slot.Amount; i++)
            {
                armySetup.Add(slot.Code);
            }
        }

        return armySetup.ToArray();
    }

    public override string ToString() => string.Concat(ToArray());

    public global::Quantum.DeckSlot[] ConvertToQuantum()
    {
        var quantumSlots = new List<global::Quantum.DeckSlot>();
        foreach (var slot in slots)
        {
            if (slot.IsEmpty) continue;
            
            quantumSlots.Add(new global::Quantum.DeckSlot(slot.Code, slot.Card.Grade, slot.Amount, slot.Card.PerkGrades));
        }

        return quantumSlots.ToArray();
    }
}
}