﻿using Wargard.Common;
using Wargard.SharedLogic.MetaModels.Cards.Decks;
using Wargard.SharedLogic.Source.GameSettings.DeckBuildingsSettings;

namespace Wargard.Player.Decks
{
public class DecksFactory
{
    private readonly ObjectsFactory objectsFactory;
    
    public DecksFactory(ObjectsFactory objectsFactory)
    {
        this.objectsFactory = objectsFactory;
    }

    public DecksSet InstantiateDeckSet(DeckSetModel model, DeckBuildingSettings settings)
    {
        var decks = new Deck[model.Decks.Length];
        for (var i = 0; i < decks.Length; i++)
        {
            decks[i] = InstantiateDeck(i, model.Decks[i], settings.CopiesInSlot);
        }
        
        var args = new object[]
        {
            decks, model, settings,
        };

        return objectsFactory.Instantiate<DecksSet>(args);
    }

    public Deck InstantiateDeck(int id, DeckModel model, int copiesInSlot)
    {
        var slots = new DeckSlot[model.Slots.Length];
        for (var i = 0; i < slots.Length; i++)
        {
            slots[i] = InstantiateDeckSlot(i, model.Slots[i], copiesInSlot);
        }
        
        var args = new object[]
        {
            id, slots, model,
        };

        return objectsFactory.Instantiate<Deck>(args);
    }

    private DeckSlot InstantiateDeckSlot(int id, DeckSlotModel model, int copiesInSlot) =>
        objectsFactory.Instantiate<DeckSlot>(new object[]
        {
            id, model, copiesInSlot,
        });
}
}