﻿using System.Collections.Generic;
using Quantum;
using UnityEngine;
using Wargard.Battle.SceneUI;
using Wargard.Battle.Signals;
using Wargard.Battle.Troops;
using Wargard.Client.Game;
using Wargard.Integrations.Analytics;
using Wargard.UI.Battle.Panels.Main;
using Zenject;

namespace Wargard.Battle.Tutorial
{
public class BattleTutorialManager : MonoBehaviour
{
    public const float MaxDistanceApplyCard = 8f;
    public const float MaxDistanceSetSquadOrderMovement = 8f;
    public const float RadiusFindTroopMultiplier = 2f;

    private SignalBus signalBus;
    private HandController handController;
    private OrderManager orderManager;
    private CameraControl cameraControl;
    private BattleManager battleManager;
    private BattleSceneUIManager sceneUI;
    private GameDirector gameDirector;
    
    private bool isWinPointsHighlighted;
    private bool isManaCostHighlighted;
    private TroopView selectedSquad;

    private int stepIndex = 1;

    [Inject]
    private void ResolveDependency(SignalBus signalBus,
                                   HandController handController,
                                   OrderManager orderManager,
                                   CameraControl cameraControl,
                                   BattleManager battleManager,
                                   BattleSceneUIManager sceneUI,
                                   GameDirector gameDirector)
    {
        this.signalBus = signalBus;
        this.handController = handController;
        this.orderManager = orderManager;
        this.cameraControl = cameraControl;
        this.battleManager = battleManager;
        this.sceneUI = sceneUI;
        this.gameDirector = gameDirector;
    }
    
    void Start()
    {
        SubscribeQuantumEvents();
        LockManagers(true);
        
        signalBus.Fire<StartTutorialSignal>();
    }

    #region Events

    private void SubscribeQuantumEvents()
    {
        QuantumEvent.Subscribe(this, (EventApplyCardTutorial e) => OnApplyCard(e));
        QuantumEvent.Subscribe(this, (EventShowDescriptionTutorial e) => OnShowDescription(e));
        QuantumEvent.Subscribe(this, (EventSetSquadOrderMovementTutorial e) => OnSetSquadOrderMovement(e));
        QuantumEvent.Subscribe(this, (EventShowSquadInfoTutorial e) => OnShowSquadInfo(e));
        QuantumEvent.Subscribe(this, (EventSetSquadOrderDefenseTutorial e) => OnSquadOrderDefense(e));
        QuantumEvent.Subscribe(this, (EventSetSquadOrderAttackTutorial e) => OnSquadOrderAttack(e));
        QuantumEvent.Subscribe(this, (EventSetSquadCancelOrderTutorial e) => OnSquadCancelOrder(e));
        QuantumEvent.Subscribe(this, (EventShowPointInfoTutorial e) => OnShowPointInfo(e));
        QuantumEvent.Subscribe(this, (EventSetPlayerArmyBotControlTutorial e) => OnSetPlayerArmyBotControl(e));
        QuantumEvent.Subscribe(this, (EventCompleteStepTutorial e) => OnCompleteStep(e));
        QuantumEvent.Subscribe(this, (EventLockPlayerActionsTutorial e) => LockManagers(e.Status));
    }

    private void OnApplyCard(EventApplyCardTutorial e)
    {
        var slotID = e.SlotID;

        if (e.IsFreeCard && handController.TryGetCurrentSlotID(out var reserveSlotID))
        {
            slotID = reserveSlotID;
        }
        
        handController.SetTutorialTargetCard(slotID, e.TargetPosition.ToUnityVector3());
        
        signalBus.Fire(new ShowTutorialStepSignal(e.Code, true));

        handController.SelectSlotTutorial(true, slotID);
        
        if (handController.TryGetCardPosition(slotID, out var startPosition))
        {
            signalBus.Fire(new ShowTutorialHandFromUIToWorldSignal(startPosition, e.TargetPosition.ToUnityVector3()));
        }

        sceneUI.HighlightTutorialTargetPoint(e.TargetPosition.ToUnityVector3());
    }

    private void OnShowDescription(EventShowDescriptionTutorial e)
    {
        signalBus.Fire(new ShowTutorialStepSignal(e.Code, true));
        
        if (e.DescriptionType == TutorialDescriptionType.Flags || e.DescriptionType == TutorialDescriptionType.FlagsAndWinPointsProgress)
        {
            foreach (var flag in battleManager.Flags)
            {
                sceneUI.HighlightTutorialFlag(flag.transform.position);
            }
        }
        if (e.DescriptionType == TutorialDescriptionType.WinPointsProgress || e.DescriptionType == TutorialDescriptionType.FlagsAndWinPointsProgress)
        {
            signalBus.Fire<ShowAndHighlightWinPointsProgressTutorialSignal>();
            isWinPointsHighlighted = true;
        }
        if (e.DescriptionType == TutorialDescriptionType.ManaCost)
        {
            handController.HighlightManaTutorial(true);
            isManaCostHighlighted = true;
        }
        if (e.DescriptionType == TutorialDescriptionType.TowersAndPortal)
        {
            foreach (var troopData in battleManager.GetBuildings(1))
            {
                sceneUI.HighlightTutorialFlag(troopData.view.Position);
            }
        }
    }

    private void OnSetSquadOrderMovement(EventSetSquadOrderMovementTutorial e)
    {
        signalBus.Fire(new ShowTutorialStepSignal(e.Code, true));
        
        if (battleManager.TryGetTroopByCode(0, e.CardEntity, out var squad))
        {
            orderManager.SetTutorialTargetTroop(squad, OrderManager.OrderType.Movement,
                                                e.TargetPosition.ToUnityVector3(), null);
            
            signalBus.Fire(
                new ShowTutorialHandFromWorldToWorldSignal(squad.view.Position, e.TargetPosition.ToUnityVector3()));
            
            sceneUI.HighlightTutorialTargetPoint(squad.view.Position);
        }

        sceneUI.HighlightTutorialTargetPoint(e.TargetPosition.ToUnityVector3());
    }

    private void OnShowSquadInfo(EventShowSquadInfoTutorial e)
    {
        signalBus.Fire(new ShowTutorialStepSignal(e.Code, true));
        
        if (battleManager.TryGetTroopByCode(e.TeamId, e.CardEntity, out var squad))
        {
            if (e.InfoType == TutorialSquadInfoType.Banner || e.InfoType == TutorialSquadInfoType.BannerAndViewRadius)
            {
                sceneUI.HighlightTutorialTargetPoint(squad.view.Position);
                selectedSquad = squad.view.GetComponent<TroopView>();
                selectedSquad.Select(true, false);
                squad.view.SelectBanner(true, false);
            }
            
            if (e.InfoType == TutorialSquadInfoType.RangeAttackRadius)
            {
                selectedSquad = squad.view.GetComponent<TroopView>();
                selectedSquad.Select(true, false);
                squad.view.SelectBanner(true, false);
            }
            
            if (e.InfoType == TutorialSquadInfoType.ViewRadius || e.InfoType == TutorialSquadInfoType.BannerAndViewRadius)
            {
                sceneUI.HighlightTutorialSquadViewRadius(squad.view.Position, squad.code);
            }
        }
    }

    private void OnSquadOrderDefense(EventSetSquadOrderDefenseTutorial e)
    {
        signalBus.Fire(new ShowTutorialStepSignal(e.Code, true));
        
        if (battleManager.TryGetTroopByCode(0, e.CardEntity, out var squad))
        {
            orderManager.SetTutorialTargetTroop(squad, OrderManager.OrderType.Defense, Vector3.zero, null);
            
            signalBus.Fire(new ShowTutorialHandDefenseSignal(squad.view.Position));
            
            sceneUI.HighlightTutorialTargetPoint(squad.view.Position);
        }
    }

    private void OnSquadOrderAttack(EventSetSquadOrderAttackTutorial e)
    {
        signalBus.Fire(new ShowTutorialStepSignal(e.Code, true));
        
        if (battleManager.TryGetTroopByCode(0, e.CardEntity, out var squad) && 
            battleManager.TryGetTroopByCode(1, e.TargetCardEntity, out var targetSquad))
        {
            orderManager.SetTutorialTargetTroop(squad, OrderManager.OrderType.Attack, Vector3.zero, targetSquad);
            
            signalBus.Fire(new ShowTutorialHandFromWorldToWorldSignal(squad.view.Position, targetSquad.view.Position));
            
            sceneUI.HighlightTutorialTargetPoint(squad.view.Position);
            sceneUI.HighlightTutorialTargetPoint(targetSquad.view.Position);
        }
    }

    private void OnSquadCancelOrder(EventSetSquadCancelOrderTutorial e)
    {
        signalBus.Fire(new ShowTutorialStepSignal(e.Code, true));
        
        if (battleManager.TryGetTroopByCode(0, e.CardEntity, out var squad))
        {
            orderManager.SetTutorialTargetTroop(squad, OrderManager.OrderType.Cancel, Vector3.zero, null);
            
            signalBus.Fire(new ShowTutorialHandTapSignal(squad.view.Position));
            
            sceneUI.HighlightTutorialTargetPoint(squad.view.Position);
        }
    }

    private void OnShowPointInfo(EventShowPointInfoTutorial e)
    {
        signalBus.Fire(new ShowTutorialStepSignal(e.Code, true));

        if (e.InfoType == TutorialPointInfoType.AllySquadDeath ||
            e.InfoType == TutorialPointInfoType.OpponentSquadDeath)
        {
            sceneUI.HighlightTutorialTargetPoint(e.Position.ToUnityVector3());
        }
    }

    private void OnSetPlayerArmyBotControl(EventSetPlayerArmyBotControlTutorial e)
    {
        signalBus.Fire(new ShowTutorialStepSignal(e.Code, true));
        
        signalBus.Fire<ShowTutorialPlayerArmyBotControlButtonSignal>();
    }

    private void OnCompleteStep(EventCompleteStepTutorial e)
    {
        handController.ClearTutorialTargetCard();
        orderManager.ClearTutorialTargetTroop();
        handController.SelectSlotTutorial(false);
        
        signalBus.Fire(new ShowTutorialStepSignal(e.Code, false));
        signalBus.Fire<HideTutorialHandSignal>();
        
        sceneUI.HideTutorialEffects();

        if (isWinPointsHighlighted)
        {
            signalBus.Fire<ToNormalWinPointsProgressTutorialSignal>();
            isWinPointsHighlighted = false;
        }
        
        if (isManaCostHighlighted)
        {
            handController.HighlightManaTutorial(false);
            isManaCostHighlighted = false;
        }

        if (selectedSquad != null)
        {
            selectedSquad.Select(false, false);
            selectedSquad.GetComponent<TroopView>().SelectBanner(false, false);
            selectedSquad = null;
        }
        
        signalBus.Fire(new AnalyticSignal(AnalyticEvents.TutorialStep, new Dictionary<string, object>
        {
            {"Code", GetStepCodeForAnalytics(e.Code)},
        }));
    }
    
    #endregion

    private void LockManagers(bool status)
    {
        handController.StartTutorial(status);
        orderManager.StartTutorial(status);
        cameraControl.Lock(status);
    }

    private void OnDestroy()
    {
        LockManagers(false);
        
        signalBus.Fire<FinishTutorialSignal>();
    }
    
    private string GetStepCodeForAnalytics(string stepCode)
    {
        if (gameDirector.CurrentTutorial == null) return "";
        
        var splittedCode = stepCode.Split('/');
        if (splittedCode.Length == 2)
        {
            stepCode = splittedCode[1];
        }
        
        var code = $"{gameDirector.CurrentTutorial.Index}_{stepIndex}_{stepCode}";
        stepIndex++;
        
        return code;
    }
}
}