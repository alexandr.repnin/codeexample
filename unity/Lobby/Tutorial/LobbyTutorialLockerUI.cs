﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Wargard.Client.Tutorial;
using Zenject;

namespace Wargard.Lobby.Tutorial
{
public class LobbyTutorialLockerUI : MonoBehaviour
{
    private enum LockType
    {
        Disable = 0,
        CanvasAlphaAndInteractable = 1,
        ButtonInteractable = 2,
    }

    [SerializeField] private LockType _lockType;
    [SerializeField] private CanvasGroup _canvasGroup;
    [SerializeField] private Button _button;
    
    private TutorialDirector tutorialDirector;
    private SignalBus signalBus;
    private bool isLocked;
    private bool isSubscribedToUnlock;
    
    [Inject]
    private void ResolveDependency(SignalBus signalBus, TutorialDirector tutorialDirector)
    {
        this.tutorialDirector = tutorialDirector;
        this.signalBus = signalBus;
    }

    private void Awake()
    {
        SubscribeToUnlockIfNeeded();
    }

    private void OnEnable()
    {
        LockIfNeeded();
    }

    private void SubscribeToUnlockIfNeeded()
    {
        if (!tutorialDirector.IsBattleInProgress) return;

        SubscribeSignals();
    }
    
    private void LockIfNeeded()
    {
        if (tutorialDirector.IsBattleInProgress == isLocked) return;

        Lock(tutorialDirector.IsBattleInProgress);
    }

    private void SubscribeSignals()
    {
        signalBus.Subscribe<RewardForCompleteTutorialReceivedSignal>(OnRewardForCompleteTutorialReceived);
        isSubscribedToUnlock = true;
    }
    
    private void UnsubscribeSignals()
    {
        if (!isSubscribedToUnlock) return;
        
        signalBus.Unsubscribe<RewardForCompleteTutorialReceivedSignal>(OnRewardForCompleteTutorialReceived);
        isSubscribedToUnlock = false;
    }

    private void OnRewardForCompleteTutorialReceived()
    {
        Lock(false);
        UnsubscribeSignals();
    }

    private void Lock(bool status)
    {
        switch (_lockType)
        {
            case LockType.Disable:
                gameObject.SetActive(!status);
                break;
            case LockType.CanvasAlphaAndInteractable:
                _canvasGroup.alpha = status ? 0f : 1f;
                _canvasGroup.interactable = !status;
                break;
            case LockType.ButtonInteractable:
                _button.interactable = !status;
                break;
        }
        
        isLocked = status;
    }

    private void OnDestroy()
    {
        UnsubscribeSignals();
    }
}
}