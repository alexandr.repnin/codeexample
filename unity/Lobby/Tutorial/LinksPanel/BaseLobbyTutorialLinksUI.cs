﻿using System;
using UnityEngine;
using Wargard.Extensions.Attributes;
using Zenject;

namespace Wargard.Lobby.Tutorial
{
public enum StepItemActionType
{
    Simple = 0,
    Click = 1,
    Drag = 2,
}

public abstract class BaseLobbyTutorialLinksUI : MonoBehaviour
{
    protected SignalBus signalBus;

    [Inject] private void ResolveDependencies(SignalBus signalBus)
    {
        this.signalBus = signalBus;
    }

    private void Start()
    {
        signalBus.Subscribe<StartStepLobbyTutorialSignal>(OnStartStepLobbyTutorialSignal);
    }

    private void OnDestroy()
    {
        signalBus.Unsubscribe<StartStepLobbyTutorialSignal>(OnStartStepLobbyTutorialSignal);
    }

    protected void FireSignalHighlightElements(CommonLobbyTutorialLinksUI.StepItem[] items) =>
        signalBus.Fire(new HighlightElementsLobbyTutorialSignal(items));

    protected abstract void OnStartStepLobbyTutorialSignal(StartStepLobbyTutorialSignal data);
    
    [Serializable]
    public class StepItem : BaseStepItem
    {
        public StepItemActionType _actionType;
        public Transform _transform;
        [ConditionalHide("_actionType", StepItemActionType.Drag)]
        public Vector3 _dragPosition;

        public StepItem(StepItemActionType actionType, Transform transform, bool isLocked)
        {
            _actionType = actionType;
            _transform = transform;
            _isLocked = isLocked;
        }

        public StepItem(StepItemActionType actionType, Transform transform, bool isLocked, Vector3 dragPosition)
            : this(actionType, transform, isLocked)
        {
            _dragPosition = dragPosition;
        }
    }
    
    public abstract class BaseStepItem
    {
        public bool _isLocked;
    }
}
}