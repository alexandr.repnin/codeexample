﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Wargard.Client.ProgressSystem.WarPass;
using Wargard.Client.UI.Lobby.WarPass;
using Zenject;

namespace Wargard.Lobby.Tutorial
{
public class BattlePassLobbyTutorialLinksUI : DynamicLobbyTutorialLinksUI
{
    private enum DynamicItemType
    {
        FreeChest = 0,
    }

    [SerializeField] private WarPassLobbyPanelViewUI _view;
    [SerializeField] private CurrentDynamicStepData[] _stepsData;
    
    [Inject] private WarPassSystem warPassSystem;

    protected override void OnStartStepLobbyTutorialSignal(StartStepLobbyTutorialSignal data)
    {
        if (TryGetStepItems(data.Code, out var stepItems))
        {
            FireSignalHighlightElements(stepItems.ToArray());
        }
    }
    
    private bool TryGetStepItems(string stepCode, out StepItem[] stepItems)
    {
        var stepItemsList = new List<StepItem>();
        
        if (TryGetStepData(_stepsData, stepCode, out var stepData))
        {
            foreach (var dynamicItem in stepData._items)
            {
                if (TryGetDynamicStepItem(dynamicItem, out var stepItem))
                {
                    stepItemsList.Add(stepItem);
                }
            }
        }

        stepItems = stepItemsList.ToArray();

        return stepItems.Length > 0;
    }

    private bool TryGetDynamicStepItem(DynamicStepItem dynamicStepItem, out StepItem stepItem)
    {
        stepItem = null;
        
        switch (dynamicStepItem._type)
        {
            case DynamicItemType.FreeChest:
                var itemTransform = _view.GetRewardTransform(warPassSystem.GetProgressItemById(0).Settings.Level);
                stepItem = new StepItem(StepItemActionType.Click, itemTransform, dynamicStepItem._isLocked);
                break;
        }

        return stepItem != null;
    }

    [Serializable]
    private class CurrentDynamicStepData : BaseDynamicStepData
    {
        public DynamicStepItem[] _items;
    }
    
    [Serializable]
    private class DynamicStepItem : BaseStepItem
    {
        public DynamicItemType _type;
    }
}
}