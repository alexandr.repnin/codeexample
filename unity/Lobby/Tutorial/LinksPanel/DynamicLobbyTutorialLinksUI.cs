﻿using System.Linq;

namespace Wargard.Lobby.Tutorial
{
public abstract class DynamicLobbyTutorialLinksUI : BaseLobbyTutorialLinksUI
{
    protected bool TryGetStepData<TDynamicStepData>(TDynamicStepData[] stepsData,
                                                    string stepCode,
                                                    out TDynamicStepData stepData)
        where TDynamicStepData : BaseDynamicStepData
    {
        stepData = stepsData.FirstOrDefault(s => s._code == stepCode);
        return stepData != null;
    }

    protected class BaseDynamicStepData
    {
        public string _code;
    }
}
}