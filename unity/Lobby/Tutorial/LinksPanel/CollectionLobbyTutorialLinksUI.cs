﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Wargard.UI.Lobby;

namespace Wargard.Lobby.Tutorial
{
public class CollectionLobbyTutorialLinksUI : DynamicLobbyTutorialLinksUI
{
    private enum DynamicItemType
    {
        SelectCollectionCard = 0,
        ClickDetailsCollectionCard = 1,
        DragToDeckCollectionCard = 2,
        SelectDeckCard = 3,
        ClickAddCopyDeckCard = 4,
    }
    
    [SerializeField] private CurrentDynamicStepData[] _stepsData;

    protected override void OnStartStepLobbyTutorialSignal(StartStepLobbyTutorialSignal data)
    {
        if (TryGetStepItems(data.Code, data.CardCode, out var stepItems))
        {
            FireSignalHighlightElements(stepItems.ToArray());
        }
    }
    
    private bool TryGetStepItems(string stepCode, string cardCode, out StepItem[] stepItems)
    {
        var stepItemsList = new List<StepItem>();
        
        if (TryGetStepData(_stepsData, stepCode, out var stepData))
        {
            foreach (var dynamicItem in stepData._items)
            {
                if (TryGetDynamicStepItem(dynamicItem, cardCode, out var stepItem))
                {
                    stepItemsList.Add(stepItem);
                }
            }
        }

        stepItems = stepItemsList.ToArray();

        return stepItems.Length > 0;
    }

    private bool TryGetDynamicStepItem(DynamicStepItem dynamicStepItem, string cardCode, out StepItem stepItem)
    {
        stepItem = null;
        
        switch (dynamicStepItem._type)
        {
            case DynamicItemType.SelectCollectionCard:
                stepItem = new StepItem(StepItemActionType.Click, GetCollectionCard(cardCode).transform, dynamicStepItem._isLocked);
                break;
            case DynamicItemType.ClickDetailsCollectionCard:
                var detailsButtonTransform = GetCollectionCard(cardCode).GetCardInfoButton().transform;
                stepItem = new StepItem(StepItemActionType.Click, detailsButtonTransform, dynamicStepItem._isLocked);
                break;
            case DynamicItemType.DragToDeckCollectionCard:
                stepItem = new StepItem(StepItemActionType.Drag, GetCollectionCard(cardCode).transform, dynamicStepItem._isLocked,
                                        GetDeckPosition());
                break;
            case DynamicItemType.SelectDeckCard:
                stepItem = new StepItem(StepItemActionType.Click, GetDeckCard(cardCode).transform, dynamicStepItem._isLocked);
                break;
            case DynamicItemType.ClickAddCopyDeckCard:
                var addCopyButtonTransform = GetDeckCard(cardCode).GetAddCopyButton().transform;
                stepItem = new StepItem(StepItemActionType.Click, addCopyButtonTransform, dynamicStepItem._isLocked);
                break;
        }

        return stepItem != null;
    }

    private CollectionCardUI GetCollectionCard(string code)
    {
        CollectionCardUI collectionCard = null;
        signalBus.Fire(new GetCollectionCardSignal(
                           code, delegate(CollectionCardUI collectionCardUI) { collectionCard = collectionCardUI; }));

        return collectionCard;
    }
    
    private Vector3 GetDeckPosition()
    {
        var position = Vector3.zero;
        signalBus.Fire(new GetDeckPositionSignal(delegate(Vector3 deckPosition) { position = deckPosition; }));

        return position;
    }
    
    private DeckCardUI GetDeckCard(string code)
    {
        DeckCardUI deckCard = null;
        signalBus.Fire(new GetDeckCardSignal(code, delegate(DeckCardUI deckCardUI) { deckCard = deckCardUI; }));

        return deckCard;
    }

    [Serializable]
    private class CurrentDynamicStepData : BaseDynamicStepData
    {
        public DynamicStepItem[] _items;
    }

    [Serializable]
    private class DynamicStepItem : BaseStepItem
    {
        public DynamicItemType _type;
    }
}
}