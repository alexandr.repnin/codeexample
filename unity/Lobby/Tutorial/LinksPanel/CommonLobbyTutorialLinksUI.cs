﻿using System;
using System.Linq;
using UnityEngine;

namespace Wargard.Lobby.Tutorial
{
public class CommonLobbyTutorialLinksUI : BaseLobbyTutorialLinksUI
{
    [SerializeField] private StepData[] _stepsData;

    protected override void OnStartStepLobbyTutorialSignal(StartStepLobbyTutorialSignal data)
    {
        if (TryGetStepData(data.Code, out var stepData))
        {
            FireSignalHighlightElements(stepData._items);
        }
    }

    private bool TryGetStepData(string stepCode, out StepData stepData)
    {
        stepData = _stepsData.FirstOrDefault(s => s._code == stepCode);
        return !stepData.Equals(default(StepData));
    }
    
    [Serializable]
    private struct StepData
    {
        public string _code;
        public StepItem[] _items;
    }
}
}