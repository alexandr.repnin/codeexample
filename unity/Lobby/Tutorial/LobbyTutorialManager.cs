﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using Wargard.Client.Tutorial;
using Wargard.Integrations.Analytics;
using Wargard.Scripts.Common;
using Wargard.UI.Common;
using Wargard.UI.Lobby;
using Zenject;

namespace Wargard.Lobby.Tutorial
{
public class LobbyTutorialManager : IDisposable
{
    private readonly TopLobbyConductorUI conductorUI;
    private readonly LobbyTutorialStepProcessorsFactory stepProcessorsFactory;
    private readonly SignalBus signalBus;
    private readonly TutorialDirector tutorialDirector;

    private AsyncAwaitTask switchStepsTask;
    private BaseLobbyTutorial currentTutorial;

    public LobbyTutorialManager(TopLobbyConductorUI conductorUI,
                                LobbyTutorialStepProcessorsFactory stepProcessorsFactory,
                                SignalBus signalBus,
                                TutorialDirector tutorialDirector)
    {
        this.conductorUI = conductorUI;
        this.stepProcessorsFactory = stepProcessorsFactory;
        this.signalBus = signalBus;
        this.tutorialDirector = tutorialDirector;
    }
    
    public void Dispose()
    {
        switchStepsTask?.Stop();
    }

    public void Start(BaseLobbyTutorial tutorial)
    {
        tutorialDirector.IsLobbyInProgress = true;

        currentTutorial = tutorial;
        
        signalBus.Fire(new AnalyticSignal(AnalyticEvents.TutorialLobbyStart, new Dictionary<string, object>
        {
            {"Code", GetTutorialCodeForAnalytics()},
        }));
        
        conductorUI.OpenTutorialPanel();

        var stepProcessors = ConvertStepsToProcessors(tutorial.Steps);
        
        switchStepsTask = new AsyncAwaitTask(token => SwitchSteps(stepProcessors, token));
        switchStepsTask.Start();
    }
    
    private void Finish()
    {
        signalBus.Fire(new AnalyticSignal(AnalyticEvents.TutorialLobbyFinish, new Dictionary<string, object>
        {
            {"Code", GetTutorialCodeForAnalytics()},
        }));
        
        conductorUI.CloseTutorialPanel();

        tutorialDirector.IsLobbyInProgress = false;
        
        signalBus.Fire<UpdateActivePanelUISignal>();
    }

    private async Task SwitchSteps(Queue<BaseLobbyTutorialStepProcessor> stepProcessors, CancellationToken token)
    {
        await Task.Delay(TimeSpan.FromSeconds(1), token);

        var stepIndex = 1;

        while (stepProcessors.Count > 0)
        {
            var currentStepProcessor = stepProcessors.Dequeue();
            if (currentStepProcessor.IsDisabled)
            {
                continue;
            }

            if (currentStepProcessor.DelayStart > 0f)
            {
                await Task.Delay(TimeSpan.FromSeconds(currentStepProcessor.DelayStart), token);
            }
            
            currentStepProcessor.Start();
            
            while (!currentStepProcessor.IsCompleted)
            {
                if (token.IsCancellationRequested)
                {
                    currentStepProcessor.StopTasks();
                    return;
                }
                
                await Task.Yield();
            }

            signalBus.Fire(new AnalyticSignal(AnalyticEvents.TutorialStep, new Dictionary<string, object>
            {
                {"Code", GetStepCodeForAnalytics(stepIndex, currentStepProcessor.Code)},
            }));
            
            stepIndex++;
        }
        
        Finish();
    }

    private Queue<BaseLobbyTutorialStepProcessor> ConvertStepsToProcessors(List<LobbyTutorialSettingsSO.Step> steps)
    {
        var processors = new Queue<BaseLobbyTutorialStepProcessor>();
        foreach (var step in steps)
        {
            if (stepProcessorsFactory.TryCreateStepProcessor(step, out var processor))
            {
                processors.Enqueue(processor);
            }
        }
        
        return processors;
    }

    private string GetTutorialCodeForAnalytics() => $"{currentTutorial.Index}_{currentTutorial.Code}";
    
    private string GetStepCodeForAnalytics(int stepIndex, string stepCode) => $"{currentTutorial.Index}_{stepIndex}_{stepCode}";
}
}
