﻿using System;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using Wargard.Scripts.Common;
using Zenject;

namespace Wargard.Lobby.Tutorial
{
public class InfoLobbyTutorialStepProcessor : BaseLobbyTutorialStepProcessor
{
    public InfoLobbyTutorialStepProcessor(LobbyTutorialSettingsSO.Step settings, SignalBus signalBus) : 
        base(settings, signalBus) { }

    public override void Start()
    {
        base.Start();

        if (settings._infoCompleteType == InfoCompleteType.ByTap)
        {
            signalBus.Subscribe<OnTapLobbyTutorialSignal>(OnTapPanel);
        }
        else if (settings._infoCompleteType == InfoCompleteType.Autocomplete)
        {
            StartAutocomplete();
        }
    }

    protected override void Finish()
    {
        base.Finish();
        
        if (settings._infoCompleteType == InfoCompleteType.ByTap)
        {
            signalBus.Unsubscribe<OnTapLobbyTutorialSignal>(OnTapPanel);
        }
    }

    public override void StopTasks()
    {
        StopAutocomplete();
    }

    #region Tap

    private void OnTapPanel(OnTapLobbyTutorialSignal data) => Finish();

    #endregion
    
    #region Autocomplete

    private AsyncAwaitTask autocompleteTask;
    
    private void StartAutocomplete()
    {
        Debug.Log($"await {settings._code}");
        
        autocompleteTask = new AsyncAwaitTask(AutocompleteTask);
        autocompleteTask.Start();
    }

    private void StopAutocomplete()
    {
        autocompleteTask?.Stop();
    }

    private async Task AutocompleteTask(CancellationToken token)
    {
        await Task.Delay(TimeSpan.FromSeconds(Mathf.Clamp(settings._autocompleteTimer, 0f, float.MaxValue)), token);

        Finish();
    }

    #endregion
}
}