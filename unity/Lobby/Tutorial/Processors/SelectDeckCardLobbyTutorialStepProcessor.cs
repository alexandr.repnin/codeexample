﻿using Wargard.UI.Lobby.Panels.Collection;
using Zenject;

namespace Wargard.Lobby.Tutorial
{
public class SelectDeckCardLobbyTutorialStepProcessor : BaseLobbyTutorialStepProcessor
{
    public SelectDeckCardLobbyTutorialStepProcessor(LobbyTutorialSettingsSO.Step settings, SignalBus signalBus) : 
        base(settings, signalBus) { }

    public override void Start()
    {
        signalBus.Subscribe<DeckCardSelectedSignal>(OnDeckCardSelectedSignal);
        
        LockActions(LockDeckActionsType.OnlyTap);

        base.Start();
    }

    protected override void Finish()
    {
        signalBus.Unsubscribe<DeckCardSelectedSignal>(OnDeckCardSelectedSignal);
        
        base.Finish();
        
        LockActions(LockDeckActionsType.Empty);
    }

    private void LockActions(LockDeckActionsType type) => signalBus.Fire(new LockDeckActionsSignal(type));

    private void OnDeckCardSelectedSignal(DeckCardSelectedSignal data) => Finish();
}
}