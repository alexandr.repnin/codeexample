﻿using Wargard.Chest;
using Zenject;

namespace Wargard.Lobby.Tutorial
{
public class CloseChestPanelLobbyTutorialStepProcessor : BaseLobbyTutorialStepProcessor
{
    public CloseChestPanelLobbyTutorialStepProcessor(LobbyTutorialSettingsSO.Step settings, SignalBus signalBus) : 
        base(settings, signalBus) { }
    
    public override void Start()
    {
        base.Start();

        signalBus.Subscribe<ChestOpenCompleteSignal>(OnChestOpenCompleteSignal);
    }

    protected override void Finish()
    {
        base.Finish();
        
        signalBus.Unsubscribe<ChestOpenCompleteSignal>(OnChestOpenCompleteSignal);
    }
    
    private void OnChestOpenCompleteSignal(ChestOpenCompleteSignal data) => Finish();
}
}