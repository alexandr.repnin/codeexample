﻿using Wargard.Common;

namespace Wargard.Lobby.Tutorial
{
public class LobbyTutorialStepProcessorsFactory
{
    private readonly ObjectsFactory objectsFactory;

    public LobbyTutorialStepProcessorsFactory(ObjectsFactory objectsFactory)
    {
        this.objectsFactory = objectsFactory;
    }

    public bool TryCreateStepProcessor(LobbyTutorialSettingsSO.Step settings, out BaseLobbyTutorialStepProcessor processor)
    {
        var args = new object[]
        {
            settings,
        };

        processor = settings._type switch
        {
            StepType.Info => objectsFactory.Instantiate<InfoLobbyTutorialStepProcessor>(args),
            StepType.ButtonClick => objectsFactory.Instantiate<ButtonClickLobbyTutorialStepProcessor>(args),
            StepType.CloseChestPanel => objectsFactory.Instantiate<CloseChestPanelLobbyTutorialStepProcessor>(args),
            StepType.SelectCollectionCard =>
                objectsFactory.Instantiate<SelectCollectionCardLobbyTutorialStepProcessor>(args),
            StepType.DragToDeckCollectionCard =>
                objectsFactory.Instantiate<DragToDeckCollectionCardLobbyTutorialStepProcessor>(args),
            StepType.SelectDeckCard =>
                objectsFactory.Instantiate<SelectDeckCardLobbyTutorialStepProcessor>(args),
            _ => null,
        };

        return processor != null;
    }
}
}