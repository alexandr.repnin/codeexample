﻿using Wargard.Extensions;
using Wargard.Integrations.Analytics;
using Zenject;

namespace Wargard.Lobby.Tutorial
{
public abstract class BaseLobbyTutorialStepProcessor
{
    protected readonly LobbyTutorialSettingsSO.Step settings;
    protected readonly SignalBus signalBus;

    public string Code => settings._code;
    public bool IsDisabled => settings._disabled;
    public float DelayStart => settings._delayStart;

    public bool IsCompleted { get; protected set; }

    protected BaseLobbyTutorialStepProcessor(LobbyTutorialSettingsSO.Step settings, SignalBus signalBus)
    {
        this.settings = settings;
        this.signalBus = signalBus;
    }

    public virtual void Start()
    {
        signalBus.Fire(
            new StartStepLobbyTutorialSignal(settings._code, settings._fadeScreen, settings._disableLockActions,
                                             settings._cardCode));
    }
    
    protected virtual void Finish()
    {
        signalBus.Fire<FinishStepLobbyTutorialSignal>();
        IsCompleted = true;
    }

    public virtual void StopTasks() { }
}
}