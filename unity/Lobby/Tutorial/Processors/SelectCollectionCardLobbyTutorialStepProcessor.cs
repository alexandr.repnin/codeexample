﻿using Wargard.UI.Lobby.Panels.Collection;
using Zenject;

namespace Wargard.Lobby.Tutorial
{
public class SelectCollectionCardLobbyTutorialStepProcessor : BaseLobbyTutorialStepProcessor
{
    public SelectCollectionCardLobbyTutorialStepProcessor(LobbyTutorialSettingsSO.Step settings, SignalBus signalBus) : 
        base(settings, signalBus) { }

    public override void Start()
    {
        signalBus.Subscribe<CollectionCardSelectedSignal>(OnCollectionCardSelectedSignal);

        LockGrid(true);
        LockActions(LockCollectionActionsType.OnlyTap);

        base.Start();
    }

    protected override void Finish()
    {
        signalBus.Unsubscribe<CollectionCardSelectedSignal>(OnCollectionCardSelectedSignal);
        
        base.Finish();
        
        LockGrid(false);
        LockActions(LockCollectionActionsType.Empty);
    }

    private void LockGrid(bool status) => signalBus.Fire(new LockCollectionGridSignal(status));

    private void LockActions(LockCollectionActionsType type) => signalBus.Fire(new LockCollectionActionsSignal(type));

    private void OnCollectionCardSelectedSignal(CollectionCardSelectedSignal data) => Finish();
}
}