﻿using Zenject;

namespace Wargard.Lobby.Tutorial
{
public class ButtonClickLobbyTutorialStepProcessor : BaseLobbyTutorialStepProcessor
{
    public ButtonClickLobbyTutorialStepProcessor(LobbyTutorialSettingsSO.Step settings, SignalBus signalBus) : 
        base(settings, signalBus) { }
    
    public override void Start()
    {
        base.Start();

        signalBus.Subscribe<OnClickButtonLobbyTutorialSignal>(OnClickButton);
    }

    protected override void Finish()
    {
        base.Finish();
        
        signalBus.Unsubscribe<OnClickButtonLobbyTutorialSignal>(OnClickButton);
    }
    
    private void OnClickButton(OnClickButtonLobbyTutorialSignal data) => Finish();
}
}