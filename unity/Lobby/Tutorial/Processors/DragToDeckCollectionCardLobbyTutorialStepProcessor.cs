﻿using Wargard.UI.Lobby;
using Wargard.UI.Lobby.Panels.Collection;
using Zenject;

namespace Wargard.Lobby.Tutorial
{
public class DragToDeckCollectionCardLobbyTutorialStepProcessor : BaseLobbyTutorialStepProcessor
{
    private CollectionCardUI currentCardUI;
    
    public DragToDeckCollectionCardLobbyTutorialStepProcessor(LobbyTutorialSettingsSO.Step settings, SignalBus signalBus) : 
        base(settings, signalBus) { }
    
    public override void Start()
    {
        signalBus.Subscribe<HighlightElementsLobbyTutorialSignal>(OnHighlightElementsLobbyTutorialSignal);
        signalBus.Subscribe<CollectionCardPutInDeckSignal>(OnCollectionCardPutInDeckSignal);
        
        LockHorizontalLG(true);
        LockGrid(true);
        LockActions(LockCollectionActionsType.OnlyDragToDeck);

        base.Start();
    }

    protected override void Finish()
    {
        signalBus.Unsubscribe<HighlightElementsLobbyTutorialSignal>(OnHighlightElementsLobbyTutorialSignal);
        signalBus.Unsubscribe<CollectionCardPutInDeckSignal>(OnCollectionCardPutInDeckSignal);
        
        base.Finish();
        
        LockHorizontalLG(false);
        LockGrid(false);
        LockActions(LockCollectionActionsType.Empty);
    }

    private void OnHighlightElementsLobbyTutorialSignal(HighlightElementsLobbyTutorialSignal data)
    {
        foreach (var item in data.Items)
        { 
            currentCardUI = item._transform.GetComponent<CollectionCardUI>();
            if (currentCardUI != null)
            {
                return;
            }
        }
    }

    private void LockGrid(bool status) => signalBus.Fire(new LockCollectionGridSignal(status));

    private void LockHorizontalLG(bool status) => signalBus.Fire(new LockCollectionHorizontalLayoutGroupSignal(status));

    private void LockActions(LockCollectionActionsType type) => signalBus.Fire(new LockCollectionActionsSignal(type));

    private void OnCollectionCardPutInDeckSignal(CollectionCardPutInDeckSignal data) => Finish();
}
}