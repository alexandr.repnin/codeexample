﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Wargard.Extensions.Attributes;
using Wargard.ScriptableObjects;

namespace Wargard.Lobby.Tutorial
{
public enum LobbyTutorialType
{
    First = 0,
    ClashDeckBuilding = 1,
    FlagCaptureDeckBuilding = 2,
}

public enum LobbyTutorialStartType
{
    Fast = 0,
    MainPanelUpdate = 1,
}

public enum StepType
{
    Info = 0,
    ButtonClick = 1,
    CloseChestPanel = 2,
    SelectCollectionCard = 3,
    DragToDeckCollectionCard = 4,
    SelectDeckCard = 5,
}

public enum InfoCompleteType
{
    ByTap = 0,
    Autocomplete = 1,
}

[CreateAssetMenu(fileName = "New_LobbyTutorial", menuName = "Data/Tutorial/Lobby", order = 0)]
public class LobbyTutorialSettingsSO : SO
{
    public string _code;
    public LobbyTutorialType _type;
    public LobbyTutorialStartType _startType;
    public List<Step> _steps;
    
    [Serializable]
    public struct Step
    {
        public string _code;
        
        [Space] public float _delayStart;
        
        [Space] public StepType _type;
        [ConditionalHide("_type", StepType.Info)]
        public InfoCompleteType _infoCompleteType;
        [ConditionalHide("_infoCompleteType", InfoCompleteType.Autocomplete)]
        public float _autocompleteTimer;
        public string _cardCode;
        
        [Space] public bool _fadeScreen;
        public bool _disableLockActions;
        
        [Space] public bool _disabled;
    }
}
}