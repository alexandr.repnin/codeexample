﻿using System;
using UnityEngine;
using Wargard.UI.Lobby;
using Wargard.UI.Lobby.Panels.Collection;

namespace Wargard.Lobby.Tutorial
{
public class StartStepLobbyTutorialSignal
{
    public readonly string Code;
    public readonly bool FadeScreen;
    public readonly bool DisableLockActions;
    public readonly string CardCode;

    public StartStepLobbyTutorialSignal(string code,
                                        bool fadeScreen,
                                        bool disableLockActions,
                                        string cardCode)
    {
        Code = code;
        FadeScreen = fadeScreen;
        DisableLockActions = disableLockActions;
        CardCode = cardCode;
    }
}

public struct FinishStepLobbyTutorialSignal { }

public struct OnTapLobbyTutorialSignal { }

public struct OnClickButtonLobbyTutorialSignal { }

public class HighlightElementsLobbyTutorialSignal
{
    public CommonLobbyTutorialLinksUI.StepItem[] Items;

    public HighlightElementsLobbyTutorialSignal(CommonLobbyTutorialLinksUI.StepItem[] items)
    {
        Items = items;
    }
}

public class GetCollectionCardSignal
{
    public string Code;
    public Action<CollectionCardUI> CardAction;

    public GetCollectionCardSignal(string code, Action<CollectionCardUI> cardAction)
    {
        Code = code;
        CardAction = cardAction;
    }
}

public struct LockCollectionActionsSignal
{
    public LockCollectionActionsType Type;

    public LockCollectionActionsSignal(LockCollectionActionsType type)
    {
        Type = type;
    }
}

public struct LockCollectionGridSignal
{
    public bool Status;

    public LockCollectionGridSignal(bool status)
    {
        Status = status;
    }
}

public struct LockCollectionHorizontalLayoutGroupSignal
{
    public bool Status;

    public LockCollectionHorizontalLayoutGroupSignal(bool status)
    {
        Status = status;
    }
}

public struct CollectionCardSelectedSignal
{
    public string Code;

    public CollectionCardSelectedSignal(string code)
    {
        Code = code;
    }
}

public struct CollectionCardPutInDeckSignal
{
    public string Code;

    public CollectionCardPutInDeckSignal(string code)
    {
        Code = code;
    }
}

public class GetDeckPositionSignal
{
    public Action<Vector3> PositionAction;

    public GetDeckPositionSignal(Action<Vector3> positionAction)
    {
        PositionAction = positionAction;
    }
}

public class GetDeckCardSignal
{
    public string Code;
    public Action<DeckCardUI> CardAction;

    public GetDeckCardSignal(string code, Action<DeckCardUI> cardAction)
    {
        Code = code;
        CardAction = cardAction;
    }
}

public struct DeckCardSelectedSignal
{
    public string Code;

    public DeckCardSelectedSignal(string code)
    {
        Code = code;
    }
}

public struct LockDeckActionsSignal
{
    public LockDeckActionsType Type;

    public LockDeckActionsSignal(LockDeckActionsType type)
    {
        Type = type;
    }
}
}